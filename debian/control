Source: compute
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Ghislain Antony Vaillant <ghisvail@gmail.com>
Section: science
Testsuite: autopkgtest
Priority: optional
Build-Depends: cmake,
               debhelper (>= 9),
               docbook-xml,
               docbook-xsl,
               doxygen,
               libboost-dev (>= 1.48),
               libboost-program-options-dev (>= 1.48),
               libboost-test-dev (>= 1.48),
               libboost-tools-dev,
               ocl-icd-opencl-dev | opencl-dev,
               xsltproc
Standards-Version: 3.9.6
Vcs-Browser: https://anonscm.debian.org/cgit/debian-science/packages/compute.git
Vcs-Git: git://anonscm.debian.org/debian-science/packages/compute.git
Homepage: http://boostorg.github.io/compute

Package: libcompute-dev
Architecture: all
Section: libdevel
Depends: ${misc:Depends},
         libboost-dev,
         ocl-icd-opencl-dev | opencl-dev
Suggests: libcompute-doc
Description: cross-platform C++ library for GPU computing
 Boost.Compute is a GPU/parallel-computing library for C++ based on OpenCL.
 .
 The core library is a thin C++ wrapper over the OpenCL API and provides access
 to compute devices, contexts, command queues and memory buffers.
 .
 On top of the core library is a generic, STL-like interface providing common
 algorithms (e.g. transform(), accumulate(), sort()) along with common
 containers (e.g. vector<T>, flat_set<T>). It also features a number of
 extensions including parallel-computing algorithms (e.g. exclusive_scan(),
 scatter(), reduce()) and a number of fancy iterators (e.g.
 transform_iterator<>, permutation_iterator<>, zip_iterator<>). 
 .
 This package provides the development files for the standalone compute 
 package.

Package: libcompute-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}
Description: documentation and examples for Boost.Compute
 Boost.Compute is a GPU/parallel-computing library for C++ based on OpenCL.
 .
 The core library is a thin C++ wrapper over the OpenCL API and provides access
 to compute devices, contexts, command queues and memory buffers.
 .
 On top of the core library is a generic, STL-like interface providing common
 algorithms (e.g. transform(), accumulate(), sort()) along with common
 containers (e.g. vector<T>, flat_set<T>). It also features a number of
 extensions including parallel-computing algorithms (e.g. exclusive_scan(),
 scatter(), reduce()) and a number of fancy iterators (e.g.
 transform_iterator<>, permutation_iterator<>, zip_iterator<>).
 .
 This package provides the documentation and example source code for the
 standalone compute package.
